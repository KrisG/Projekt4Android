package com.example.kgawl.projekt4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by kgawl on 10.01.2017.
 */

public class DatabaseProvider extends SQLiteOpenHelper {

    final String Init = "CREATE TABLE Markers(Id INTEGER PRIMARY KEY AUTOINCREMENT, Latitude REAL," +
            " Longitude REAL, Description TEXT, Title TEXT);";

    public DatabaseProvider(Context context)
    {
        super(context, "MarkerDatabase.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Init);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void InsertMarker(String title, String description, double longitude, double latitude){
        ContentValues values = new ContentValues();
        values.put("Description", description);
        values.put("Title", title);
        values.put("Latitude", latitude);
        values.put("Longitude", longitude);
        try {
            this.getWritableDatabase().insertOrThrow("Markers", "", values);
        }
        catch(Exception ex)
        {}
    }

    public ArrayList<Marker> GetAllMarkers(){
        ArrayList<Marker> markers = new ArrayList<>();

        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT Description, Longitude, Latitude, Title FROM Markers", null);

        while(cursor.moveToNext()){
            Marker temp = new Marker();
            temp.Description = cursor.getString(0);
            temp.Longitude = cursor.getDouble(1);
            temp.Latitude = cursor.getDouble(2);
            temp.Title = cursor.getString(3);
            markers.add(temp);
        }

        return markers;
    }

    public void DeleteMarkers() {
        this.getWritableDatabase().execSQL("Delete from Markers");
    }
}
