package com.example.kgawl.projekt4;

/**
 * Created by kgawl on 10.01.2017.
 */

public class Marker {
    public String Title;
    public String Description;
    public double Latitude;
    public double Longitude;
}
