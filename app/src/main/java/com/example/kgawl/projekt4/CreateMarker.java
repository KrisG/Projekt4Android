package com.example.kgawl.projekt4;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.Set;

public class CreateMarker extends AppCompatActivity {

    EditText placeName;
    EditText placeDescription;

    double Latitude;
    double Longitude;

    DatabaseProvider db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_marker);

        placeName = (EditText) findViewById(R.id.editText);
        placeDescription = (EditText) findViewById(R.id.editText2);

        Intent reciver = getIntent();

        Latitude = reciver.getExtras().getDouble("Latitude");
        Longitude = reciver.getExtras().getDouble("Longitude");

        db = new DatabaseProvider(this.getApplicationContext());
    }

    public void SaveMarker(View v){
        db.InsertMarker(placeName.getText().toString() , placeDescription.getText().toString(), Longitude, Latitude);

        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }
}